# Paddle

![paddle](paddle_logo.png "paddle logo")

Progressive Assembly by the Directed Development of Long rEads

Note: this uses the same concept as [RaynBow](https://gitlab.com/gringer/raynbow) 
and [IMAGE](http://www.sanger.ac.uk/resources/software/pagit/#IMAGE), but using 
Canu for the assembler (instead of Ray / Velvet) and minimap2 for the mapper 
(instead of Bowtie2 / SMALT).