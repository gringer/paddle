#!/bin/bash

## PADDLE - Progressive Assembly by the Directed Development of Long rEads
## Copyright (C) 2019  David Eccles (gringer)
##
## This program is free software: you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation, either version 3 of the License, or
## (at your option) any later version.

## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.

## You should have received a copy of the GNU General Public License
## along with this program.  If not, see <http://www.gnu.org/licenses/>.

## usage: ./paddle.sh <readset.fq.gz> <iteration #>

readset=$1;
pass=$2;

canu_cmd="~/install/canu/canu-1.8/Linux-amd64/bin/canu";
minimap2_cmd="~/install/minimap2/minimap2";
genomeSize="15M";
nthreads="10";

## Extract reads
samtools fastq <(pv mapped_stroke_$((${pass}-1)).bam) | gzip > paddle_stroke_${pass}.fq.gz
## Assemble
${canu_cmd} -nanopore-raw paddle_stroke_${pass}.fq.gz -p paddle_${pass} -d paddle_${pass} genomeSize=${genomeSize} stopOnLowCoverage=0
## Fish out matching contigs
${minimap2_cmd} -x ava-ont paddle_${pass}/paddle_${pass}.contigs.fasta fishing_sequence.fa | awk '{print $6}' | sort | uniq > fished_contigs.txt
## Fish out contigs that cling to the matching contigs
(cat fished_contigs.txt; grep -F -f fished_contigs.txt paddle_${pass}/paddle_${pass}.contigs.gfa | \
   awk '{print $2"\n"$4}' | grep -v '^LN') | sort | uniq > fished_temp.txt
while [ $(ls -l fished*.txt | awk '{print $5}' | uniq | wc -l) != 1 ];
  do echo "Fishing for more contigs";
  sleep 0.1;
  cp fished_temp.txt fished_contigs.txt;
  (cat fished_contigs.txt; grep -F -f fished_contigs.txt paddle_${pass}/paddle_${pass}.contigs.gfa | \
     awk '{print $2"\n"$4}' | grep -v '^LN') | sort | uniq > fished_temp.txt
done
## Retrieve sequences for the matching contigs
samtools faidx paddle_${pass}/paddle_${pass}.contigs.fasta $(cat fished_contigs.txt) > paddle_${pass}/paddle_${pass}.matchOnly.fasta
## Map reads to matching contigs
${minimap2_cmd} -x map-ont -t ${nthreads} -p 1 -a paddle_${pass}/paddle_${pass}.matchOnly.fasta \
  ${readset} | samtools view -h -F 0x04 | samtools sort > mapped_stroke_${pass}.bam
